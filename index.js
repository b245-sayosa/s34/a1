const express = require("express");

const port = 4000;


const app = express();


app.use(express.json());


let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];

//get/home
app.get("/home", (request, response) => {
			response.status(200).send("This is Home! screen /items for Items List")
		})

//get items
app.get("/items", (request, response) =>{

			response.send(items);
		})
//delete items
app.delete("/delete-item", (request, response)=>{

			let deletedItem = items.pop();

			response.send(deletedItem);
		})




app.listen(port, ()=> console.log(`Server is running at port ${port}!`))